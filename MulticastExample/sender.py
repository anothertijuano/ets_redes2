import socket
import struct
import sys

message = 'Hello im the Sender!'.encode()
multicast_group = ('224.3.29.71', 10000)

# Create the datagram socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Set a timeout so the socket does not block indefinitely when trying
# to receive data.
sock.settimeout(1)

# Set the time-to-live for messages to 1 so they do not go past the
# local network segment.
ttl = struct.pack('b', 3)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

try:

    # Send data to the multicast group
    print('sending {}'.format(message), file=sys.stderr)
    sent = sock.sendto(message, multicast_group)

    # Look for responses from all recipients
    while True:
        print('Waiting to receive', file=sys.stderr)
        try:
            data, server = sock.recvfrom(16)
        except socket.timeout:
            print('Timed out, no more responses', file=sys.stderr)
            break
        else:
            print('Received {} from {}'.format(data, server), file=sys.stderr)

finally:
    print('Closing socket', file=sys.stderr)
    sock.close()

